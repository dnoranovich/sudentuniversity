-- https://www.eiuc.org/education/ema/being-an-ema-student/students/list-of-students.html
-- http://www.law.utoronto.ca/academic-programs/course-calendar

-- A scripts for the creation of a table to store students data.
CREATE TABLE students(
    id INT PRIMARY KEY auto_increment NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    year_of_study INT NOT NULL
);

-- A scripts for the creation of a table to store courses data.
CREATE TABLE courses(
    id INT PRIMARY KEY auto_increment NOT NULL,
    course_name VARCHAR(255) UNIQUE NOT NULL,
    course_description VARCHAR(2048) NOT NULL,
    course_credits INT NOT NULL
);

-- A ling table to implement Many-to-Many relationship beteween students and 
-- courses.
CREATE TABLE students_courses(
    student_id INT NOT NULL REFERENCES students(id),
    course_id INT NOT NULL REFERENCES courses(id),
    UNIQUE KEY enrollment (student_id, course_id)
);

-- Sample students data.
INSERT INTO students(id, first_name, last_name, email, year_of_study) VALUES
(1, 'Aayushi', 'Aggarwal', 'aggarwal@uni.ca', 1),
(2, 'Cecilia Binwi', 'Amabo', 'amabo@uni.ca', 2),
(3, 'Georg', 'Bauer', 'bauer@uni.ca', 3),
(4, 'Francesca Catalina', 'Coletti', 'coletti@uni.ca', 4),
(5, 'Emma Louise', 'Douglas', 'douglas@uni.ca', 2);

-- Sample courses data.
INSERT INTO courses(id, course_name, course_description, course_credits) VALUES
(1, 'Aboriginal Peoples and Canadian Criminal Justice',
'Aboriginal people are significantly and tragically overrepresented in Canada''s prisons and criminal courts. This is the result of numerous factors, some of which are intrinsic to Canada''s criminal justice system, and some of which are extrinsic to that system. This course will examine the substantive law and historical and cultural context for Canadian criminal law, as it applies to Aboriginal people. Throughout the course, the instructors will discuss advocacy and ethical issues that arise for counsel who represent Aboriginal clients in any litigation proceedings, criminal or otherwise.', 3),
(2, 'Business Organizations',
'This course is concerned with the law of business corporations. The purpose of the course is to provide the student with an understanding of the basic principles of modern business corporations law in light of the current statutory regimes and evolving case law. Particular emphasis will be placed on the role of the business corporation in modern society, and on the functions and responsibilities of the officers and directors in the context of different corporate transactions. The subject necessarily involves both a pragmatic or functional look at the modern corporation as well as a theoretical or jurisprudential examination of the corporation and the parties interested in its operation.', 4),
(3, 'Clinical Legal Education Asper Centre for Constitutional Rights',
'Students participating in clinical programs are encouraged to take opportunities to integrate their clinical work into an upper year paper course. Students must obtain approval from the Clinical Director, the paper course instructor, and Assistant Dean, JD Program. This course offers students the opportunity to engage in Charter rights advocacy, including but not limited to litigation, under the supervision of experienced lawyers.', 4),
(4, 'Environmental Law',
'A survey course of environmental law and policy with a particular emphasis on Ontario law and federal law. Topics will include pollution control, climate change, environmental assessment and corporate environmental responsibility. We will also examine the economic and scientific background to environmental issues and consider problems with current regulatory and liability regimes.', 3),
(5, 'Evidence Law',
'This course provides an introduction to the basic principles of the law of evidence. Common law and statutory rules of evidence are analyzed in light of the adversary system, the Canadian Charter of Rights and Freedoms, and other social values. While both civil and criminal evidence issues are considered, the focus is on the admissibility of evidence in criminal cases. Topics to be covered may include: relevancy, testimonial competence, the rule against hearsay, opinion evidence, character evidence, privilege and related matters, confessions, and illegally obtained evidence.', 4);
