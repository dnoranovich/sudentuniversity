/*
 * The MIT License
 *
 * Copyright 2017 Dmitry Noranovich javaeeeee (at) gmail (dot) com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.javaeeeee.quicktapsurvey.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A class to store student data
 *
 * @author Dmitry Noranovich javaeeeee (at) gmail (dot) com
 */
public class Student {

    /**
     * Unique id of a student.
     */
    private Integer id;
    /**
     * Student's first name.
     */
    private String firstName;
    /**
     * Student's last name.
     */
    private String lastName;
    /**
     * Student's last e-mail.
     */
    private String email;
    /**
     * Student's year of study.
     */
    private Integer year;
    /**
     * Courses a student is enrolled in;
     */
    private List<Course> courses = new ArrayList<>();

    public Student() {
    }

    public Student(Integer id, String firstName,
            String lastName, String email, Integer year) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.year = year;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public List<Course> getCourses() {
        return courses;
    }

    /**
     * A method to add a course taken by a student.
     *
     * @param course course taken by a student
     */
    public void addCourse(Course course) {
        courses.add(course);
    }

    public void addCourses(List<Course> courses) {
        if (this.courses != null) {
            this.courses.clear();
        }
        this.courses = courses;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.firstName,
                this.lastName, this.email, this.year);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Student other = (Student) obj;
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.year, other.year)) {
            return false;
        }
        return true;
    }

}
