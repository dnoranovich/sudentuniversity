/*
 * The MIT License
 *
 * Copyright 2017 Dmitry Noranovich javaeeeee (at) gmail (dot) com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.javaeeeee.quicktapsurvey.view;

import java.io.Serializable;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 * This bean serves left-hand side menu.
 *
 * @author Dmitry Noranovich javaeeeee (at) gmail (dot) com
 */
@Named(value = "navigationManagedBean")
@ViewScoped
public class NavigationManagedBean implements Serializable {

    /**
     * Variable to show if students panel is visible.
     */
    private boolean isStudentsVisisble = true;
    /**
     * Variable to show if courses panel is visible.
     */
    private boolean isCoursesVisible = false;
    /**
     * Variable to show if tree operations panel is visible.
     */
    private boolean isTreesVisible = false;

    /**
     * Creates a new instance of NavigationManagedBean
     */
    public NavigationManagedBean() {
    }

    /**
     * Action listener to show students panel.
     */
    public void showStudentsPanel() {
        isStudentsVisisble = true;
        isCoursesVisible = false;
        isTreesVisible = false;
    }

    /**
     * Action listener to show courses panel.
     */
    public void showCourcesPanel() {
        isStudentsVisisble = false;
        isCoursesVisible = true;
        isTreesVisible = false;
    }

    /**
     * Action listener to show trees panel.
     */
    public void showTreesPanel() {
        isStudentsVisisble = false;
        isCoursesVisible = false;
        isTreesVisible = true;
    }

    public boolean isIsStudentsVisisble() {
        return isStudentsVisisble;
    }

    public boolean isIsCoursesVisible() {
        return isCoursesVisible;
    }

    public boolean isIsTreesVisible() {
        return isTreesVisible;
    }

}
