/*
 * The MIT License
 *
 * Copyright 2017 Dmitry Noranovich javaeeeee (at) gmail (dot) com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.javaeeeee.quicktapsurvey.view;

import com.javaeeeee.quicktapsurvey.businesslogic.StudentSessionBean;
import com.javaeeeee.quicktapsurvey.entities.Course;
import com.javaeeeee.quicktapsurvey.entities.Student;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Dmitry Noranovich javaeeeee (at) gmail (dot) com
 */
@Named(value = "studentsManagedBean")
@ViewScoped
public class StudentsManagedBean implements Serializable {

    /**
     * A bean with Student-related business logic is injected.
     */
    @EJB
    private StudentSessionBean studentSessionBean;
    /**
     * A field to store student data.
     */
    private Student student;
    /**
     * A field to store a selected course.
     */
    private Course course;

    /**
     * Creates a new instance of UsersManagedBean
     */
    public StudentsManagedBean() {
    }

    /**
     * Method returns list of all students.
     *
     * @return list of all students.
     */
    public List<Student> getStudents() {
        return studentSessionBean.listStudents();
    }

    /**
     * Method to add a new student.
     */
    public void addStudent() {
        student = studentSessionBean.saveStudent(student);
    }

    /**
     * A method to create a new student.
     */
    public void createStudent() {
        student = new Student();
    }

    /**
     * Method retrieves the list of courses for a student.
     */
    public void updateStudentWithCourseData(final Student myStudent) {
        myStudent.addCourses(
                studentSessionBean
                        .findCoursesAStudentIsEnrolledInByTheStudentId(
                                myStudent.getId()));
    }

    /**
     * Method used to add a course to the list of courses a student is enrolled
     * in.
     */
    public void enroll() {
        studentSessionBean.addCourseToStdent(student.getId(), course.getId());
    }

    /**
     * A method to delete a student from the database.
     *
     * @param student A student to delete.
     */
    public void deleteStudent(final Student student) {
        studentSessionBean.deleteStudent(student.getId());
    }

    /**
     * A method to set student field to the selected student value.
     *
     * @param selectEvent Event passed to this method on row double click.
     */
    public void selectStudent(final SelectEvent selectEvent) {
        student = (Student) selectEvent.getObject();
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(final Student student) {
        this.student = student;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(final Course course) {
        this.course = course;
    }

}
