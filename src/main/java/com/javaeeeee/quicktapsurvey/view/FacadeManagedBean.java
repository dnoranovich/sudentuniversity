/*
 * The MIT License
 *
 * Copyright 2017 Dmitry Noranovich javaeeeee (at) gmail (dot) com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.javaeeeee.quicktapsurvey.view;

import com.javaeeeee.quicktapsurvey.businesslogic.CourseSessionBean;
import com.javaeeeee.quicktapsurvey.entities.Course;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 * Facade Bean providing data to converters.
 *
 * @author Dmitry Noranovich javaeeeee (at) gmail (dot) com
 */
@ManagedBean(name = "facadeManagedBean", eager = true)
@ApplicationScoped
public class FacadeManagedBean {

    /**
     * Inject EJB with course data.
     */
    @EJB
    private CourseSessionBean courseSessionBean;

    /**
     * Creates a new instance of FacadeManagedBean
     */
    public FacadeManagedBean() {
    }

    /**
     * Method finds a course by its unique id.
     *
     * @param id The unique id of a course
     * @return A course characterized by id or null if not found.
     */
    public Course findCourseById(Integer id) {
        return courseSessionBean.findCourseById(id);
    }

}
