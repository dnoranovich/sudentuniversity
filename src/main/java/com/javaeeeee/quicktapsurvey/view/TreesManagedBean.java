/*
 * The MIT License
 *
 * Copyright 2017 Dmitry Noranovich javaeeeee (at) gmail (dot) com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.javaeeeee.quicktapsurvey.view;

import com.javaeeeee.quicktapsurvey.trees.Tree;
import com.javaeeeee.quicktapsurvey.view.exceptions.TreeGenerationException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 * A class to work with trees.
 *
 * @author Dmitry Noranovich javaeeeee (at) gmail (dot) com
 */
@Named(value = "treesManagedBean")
@ViewScoped
public class TreesManagedBean implements Serializable {

    /**
     * A variable to store string representation of a tree.
     */
    private String treeData = "4,5,9,8,6,,5,2,7,,,,8";
    /**
     * A number to look for in the tree.
     */
    private int number = 5;

    /**
     * A variable to store a tree.
     */
    private Tree tree = Tree.createTree(null);

    /**
     * A variable to store the found data.
     */
    private List<Integer> levels = new ArrayList<>();

    /**
     * A variable to disable the Find button in case no tree was generated.
     */
    private boolean findDisabled = true;

    /**
     * Creates a new instance of TreesManagedBean
     */
    public TreesManagedBean() {
    }

    /**
     * A method used to parse the string representation of a tree.
     *
     * @return A double-ended queue containing numbers.
     * @throws TreeGenerationException
     */
    private Deque<Integer> convertStringToQueue(final String data) {
        String parsedString = data.replaceAll(" ", "")
                .replaceAll("[,]+", ",");
        String[] array = parsedString.split(",");
        Deque<Integer> result = new LinkedList<>();
        for (String str : array) {
            result.add(Integer.parseInt(str));
        }
        return result;
    }

    /**
     * A method to generate a tree from a string representation.
     */
    public void generateTree() {
        tree = Tree.createTree(convertStringToQueue(treeData));
        findDisabled = false;
    }

    /**
     * A Method to find a number in the tree.
     */
    public void findLevels() {
        levels = tree.findLevels(number);
    }

    public String getTreeData() {
        return treeData;
    }

    public void setTreeData(String treeData) {
        this.treeData = treeData;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Tree getTree() {
        return tree;
    }

    public List<Integer> getLevels() {
        return levels;
    }

    public boolean isFindDisabled() {
        return findDisabled;
    }

}
