/*
 * The MIT License
 *
 * Copyright 2017 Dmitry Noranovich javaeeeee (at) gmail (dot) com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.javaeeeee.quicktapsurvey.trees;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * A class to store tree data.
 *
 * @author Dmitry Noranovich javaeeeee (at) gmail (dot) com
 */
public class Tree {

    private Node root = null;

    private Tree() {
    }

    /**
     * A method to create a Tree from a double-ended queue filled with numbers.
     *
     * @param numbersDeque a double-ended queue filled with numbers.
     * @return a tree populated with numbers.
     */
    public static Tree createTree(final Deque<Integer> numbersDeque) {
        if (numbersDeque == null || numbersDeque.isEmpty()) {
            return new Tree();
        }
        Tree result = new Tree();
        Deque<Node> nodesDeque = new LinkedList<>();
        Node node = new Node(numbersDeque.poll());
        node.setLevel(1);
        result.setRoot(node);
        nodesDeque.add(node);

        Node left, right;
        int currentLevel;
        while (!numbersDeque.isEmpty()) {
            node = nodesDeque.poll();
            currentLevel = node.getLevel() + 1;

            if (numbersDeque.isEmpty()) {
                break;
            }
            left = new Node(numbersDeque.poll());
            left.setLevel(currentLevel);
            node.setLeft(left);
            nodesDeque.add(left);

            if (numbersDeque.isEmpty()) {
                break;
            }
            right = new Node(numbersDeque.poll());
            right.setLevel(currentLevel);
            node.setRight(right);
            nodesDeque.add(right);
        }

        return result;
    }

    public List<Integer> getTreeInLevelOrder() {
        List<Integer> result = new ArrayList<>();

        if (!isEmpty()) {
            Deque<Node> deque = new LinkedList<>();
            deque.add(root);
            Node node;
            while (!deque.isEmpty()) {
                node = deque.poll();
                result.add(node.getData());
                if (node.getLeft() != null) {
                    deque.add(node.getLeft());
                }
                if (node.getRight() != null) {
                    deque.add(node.getRight());
                }
            }
        }

        return result;
    }

    /**
     * Convenience method to check if the tree contains any elements.
     *
     * @return true if the tree doesn't contain any elements.
     */
    public boolean isEmpty() {
        return root == null;
    }

    /**
     * Method finds at what levels in the tree the provided number exists. A
     * breadth-first search is used because as seen from the example provided
     * the tree is not a typical "sorted" binary tree, but a tree-like structure
     * where elements larger then the root can be found on the left of the tree
     * root. Also, the example tree contains repeated elements. So, the
     * comparison approach won't work here.
     *
     * @param number a number to look for.
     * @return List of tree levels at which the number was found.
     */
    public List<Integer> findLevels(int number) {
        List<Integer> result = new ArrayList<>();
        if (!isEmpty()) {
            Deque<Node> deque = new LinkedList<>();
            deque.add(root);
            Node node;
            while (!deque.isEmpty()) {
                node = deque.poll();
                if (node.getData() == number) {
                    result.add(node.getLevel());
                }
                
                if (node.getLeft() != null) {
                    deque.add(node.getLeft());
                }
                if (node.getRight() != null) {
                    deque.add(node.getRight());
                }
            }
        }
        return result;
    }

    public Node getRoot() {
        return root;
    }

    private void setRoot(Node root) {
        this.root = root;
    }
}
