/*
 * The MIT License
 *
 * Copyright 2017 Dmitry Noranovich javaeeeee (at) gmail (dot) com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.javaeeeee.quicktapsurvey.businesslogic;

import com.javaeeeee.quicktapsurvey.entities.Course;
import com.javaeeeee.quicktapsurvey.entities.Student;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;

/**
 * Class with methods to manipulate student.
 *
 * @author Dmitry Noranovich javaeeeee (at) gmail (dot) com
 */
@Stateless
public class StudentSessionBean extends UtilitySessionBean {

    /**
     * Method returns the list of all students stored in the database.
     *
     * @return list of all students stored in the database.
     */
    public List<Student> listStudents() {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        List<Student> result = new ArrayList<>();

        try {
            connection = dataSource.getConnection();
            preparedStatement
                    = connection.prepareStatement("SELECT * FROM students");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Student student = new Student(resultSet.getInt("id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("email"),
                        resultSet.getInt("year_of_study"));
                result.add(student);
            }

            resultSet.close();
        } catch (SQLException ex) {
            Logger.getLogger(
                    StudentSessionBean.class.getName())
                    .log(Level.SEVERE, null, ex);
        } finally {
            cleanUp(preparedStatement, connection);
        }

        return result;
    }

    /**
     * A method to save student data.
     *
     * @param student An object containing data to save.
     * @return The unique id of the student saved.
     */
    public Student saveStudent(final Student student) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            if (student.getId() == null) {
                //http://stackoverflow.com/questions/1915166/how-to-get-the-insert-id-in-jdbc
                preparedStatement = connection.prepareStatement(
                        "INSERT INTO students"
                        + "(id, first_name, last_name, email, year_of_study) "
                        + "VALUES "
                        + "(null, ?, ?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, student.getFirstName());
                preparedStatement.setString(2, student.getLastName());
                preparedStatement.setString(3, student.getEmail());
                preparedStatement.setInt(4, student.getYear());

                int numInsertedRows = preparedStatement.executeUpdate();
                if (numInsertedRows == 0) {
                    throw new SQLException("Failed to insert a new student.");
                }

                try (ResultSet keys = preparedStatement.getGeneratedKeys()) {
                    if (keys.next()) {
                        student.setId(keys.getInt(1));
                    } else {
                        throw new SQLException("Failed to extract student id.");
                    }
                }

            } else {
                preparedStatement
                        = connection.prepareStatement(
                                "UPDATE students "
                                + "SET first_name = ?, "
                                + "last_name = ?, "
                                + "email = ?, "
                                + "year_of_study = ? "
                                + "WHERE id = ?"
                        );
                preparedStatement.setString(1, student.getFirstName());
                preparedStatement.setString(2, student.getLastName());
                preparedStatement.setString(3, student.getEmail());
                preparedStatement.setInt(4, student.getYear());
                preparedStatement.setInt(5, student.getId());
                int numInserted = preparedStatement.executeUpdate();
                if (numInserted != 1) {
                    throw new SQLException(
                            "Can't update student data.");
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentSessionBean.class.getName())
                    .log(Level.SEVERE, null, ex);
        } finally {
            cleanUp(preparedStatement, connection);
        }
        return student;
    }

    /**
     * A method to find all courses that a student is enrolled in.
     *
     * @param id The unique id of a student.
     * @return List of all courses that a student is enrolled in.
     */
    public List<Course> findCoursesAStudentIsEnrolledInByTheStudentId(
            final Integer id
    ) {
        List<Course> result = new ArrayList<>();
        if (id != null) {
            Connection connection = null;
            PreparedStatement preparedStatement = null;
            try {
                connection = dataSource.getConnection();

                preparedStatement = connection.prepareStatement(
                        "SELECT c.id, c.course_name, "
                        + "c.course_description, c.course_credits "
                        + "FROM courses c, students s, students_courses sc "
                        + "WHERE s.id = ? AND c.id = sc.course_id AND "
                        + "s.id = sc.student_id"
                );
                preparedStatement.setInt(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();

                extractCourseListFromResultSet(resultSet, result);

            } catch (SQLException ex) {
                Logger.getLogger(StudentSessionBean.class.getName())
                        .log(Level.SEVERE, null, ex);
            } finally {
                cleanUp(preparedStatement, connection);
            }
        }
        return result;
    }

    public void addCourseToStdent(Integer studentId, Integer courseID) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            preparedStatement
                    = connection.prepareStatement(
                            "INSERT INTO students_courses"
                            + "(student_id, course_id) VALUES "
                            + "(?, ?)"
                    );
            preparedStatement.setInt(1, studentId);
            preparedStatement.setInt(2, courseID);
            int numInserted = preparedStatement.executeUpdate();
            if (numInserted == 0) {
                throw new SQLException("Can't enroll a student for a course.");
            }

        } catch (SQLException ex) {
            Logger.getLogger(
                    StudentSessionBean.class.getName())
                    .log(Level.SEVERE, null, ex);
        } finally {
            cleanUp(preparedStatement, connection);
        }
    }

    /**
     * A method to remove a student from the database.
     *
     * @param id Unique id describing a student.
     */
    public void deleteStudent(final Integer id) {
        PreparedStatement preparedStatement = null;
        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            // Delete a student.
            preparedStatement
                    = connection.prepareStatement(
                            "DELETE FROM students "
                            + "WHERE id = ?"
                    );
            preparedStatement.setInt(1, id);
            int numInserted = preparedStatement.executeUpdate();
            if (numInserted == 0) {
                throw new SQLException(
                        "Can't delete a student from the database.");
            }

            // Delete student's is enrollments.
            preparedStatement
                    = connection.prepareStatement(
                            "DELETE FROM students_courses "
                            + "WHERE student_id = ?"
                    );
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(
                    StudentSessionBean.class.getName())
                    .log(Level.SEVERE, null, ex);
        } finally {
            cleanUp(preparedStatement, connection);
        }
    }
}
