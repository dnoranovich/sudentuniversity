/*
 * The MIT License
 *
 * Copyright 2017 Dmitry Noranovich javaeeeee (at) gmail (dot) com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.javaeeeee.quicktapsurvey.businesslogic;

import com.javaeeeee.quicktapsurvey.entities.Course;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * A bean containing common methods to work with database using JDBC.
 *
 * @author Dmitry Noranovich javaeeeee (at) gmail (dot) com
 */
public class UtilitySessionBean {

    /**
     * An object that facilitates database operations.
     */
    @Resource(mappedName = "jdbc/myDatasource")
    protected DataSource dataSource;

    /**
     * A method that does cleanup for a database connection.
     *
     * @param preparedStatement Prepared Statement.
     * @param connection Database Connection.
     */
    protected void cleanUp(final PreparedStatement preparedStatement,
            final Connection connection) {
        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(
                        StudentSessionBean.class
                                .getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(
                        StudentSessionBean.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * A method to extract course data from a result set and populate a list of
     * courses with it.
     *
     * @param resultSet The result set with courses data.
     * @param result List of courses to populate.
     * @throws SQLException An exception if something goes wrong.
     */
    public void extractCourseListFromResultSet(final ResultSet resultSet,
            final List<Course> result) throws SQLException {
        while (resultSet.next()) {
            Course course = extractCourseFromResultSetData(resultSet);
            result.add(course);
        }
    }

    public Course extractCourseFromResultSetData(final ResultSet resultSet)
            throws SQLException {
        Course course = new Course(resultSet.getInt("id"),
                resultSet.getString("course_name"),
                resultSet.getInt("course_credits"),
                resultSet.getString("course_description"));
        return course;
    }
}
