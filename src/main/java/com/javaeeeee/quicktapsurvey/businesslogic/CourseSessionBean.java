/*
 * The MIT License
 *
 * Copyright 2017 Dmitry Noranovich javaeeeee (at) gmail (dot) com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.javaeeeee.quicktapsurvey.businesslogic;

import com.javaeeeee.quicktapsurvey.entities.Course;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;

/**
 *
 * @author Dmitry Noranovich javaeeeee (at) gmail (dot) com
 */
@Stateless
public class CourseSessionBean extends UtilitySessionBean {

    /**
     * Method returns list of all courses stored in the database.
     *
     * @return list of all courses stored in the database.
     */
    public List<Course> listCourses() {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        List<Course> result = new ArrayList<>();

        try {
            connection = dataSource.getConnection();
            preparedStatement
                    = connection.prepareStatement("SELECT * FROM courses");
            ResultSet resultSet = preparedStatement.executeQuery();

            extractCourseListFromResultSet(resultSet, result);

            resultSet.close();
        } catch (SQLException ex) {
            Logger.getLogger(CourseSessionBean.class.getName())
                    .log(Level.SEVERE, null, ex);
        } finally {
            cleanUp(preparedStatement, connection);
        }

        return result;
    }

    /**
     * A method to save course data.
     *
     * @param course An object containing data to save.
     * @return The unique id of the course saved.
     */
    public Course saveCourse(final Course course) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            if (course.getId() == null) {
                preparedStatement = connection.prepareStatement(
                        "INSERT INTO courses"
                        + "(id, course_name, course_description, course_credits) "
                        + "VALUES"
                        + "(null, ?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, course.getName());
                preparedStatement.setString(2, course.getDescription());
                preparedStatement.setInt(3, course.getCredits());

                int numInsertedRows = preparedStatement.executeUpdate();
                if (numInsertedRows == 0) {
                    throw new SQLException("Failed to insert a new course.");
                }

                try (ResultSet keys = preparedStatement.getGeneratedKeys()) {
                    if (keys.next()) {
                        course.setId(keys.getInt(1));
                    } else {
                        throw new SQLException("Failed to extract course id.");
                    }
                }

            } else {
                // TODO: Implement course update functionality.
            }
        } catch (SQLException ex) {
            Logger.getLogger(CourseSessionBean.class.getName())
                    .log(Level.SEVERE, null, ex);
        } finally {
            cleanUp(preparedStatement, connection);
        }
        return course;
    }

    /**
     * Method finds a course by its unique id.
     *
     * @param id The unique id of a course
     * @return A course characterized by id or null if not found.
     */
    public Course findCourseById(final Integer id) {
        if (id == null) {
            return null;
        }
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        Course course = null;

        try {
            connection = dataSource.getConnection();
            preparedStatement
                    = connection.prepareStatement("SELECT * FROM courses "
                            + "WHERE id = ?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                course = extractCourseFromResultSetData(resultSet);
            }

            resultSet.close();
        } catch (SQLException ex) {
            Logger.getLogger(CourseSessionBean.class.getName())
                    .log(Level.SEVERE, null, ex);
        } finally {
            cleanUp(preparedStatement, connection);
        }
        return course;
    }
}
