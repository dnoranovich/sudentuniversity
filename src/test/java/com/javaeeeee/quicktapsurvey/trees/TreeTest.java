/*
 * The MIT License
 *
 * Copyright 2017 Dmitry Noranovich javaeeeee (at) gmail (dot) com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.javaeeeee.quicktapsurvey.trees;

import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Dmitry Noranovich javaeeeee (at) gmail (dot) com
 */
public class TreeTest {

    @Test
    public void shouldCreateAnEmptyTreeFromNull() {
        Tree tree = Tree.createTree(null);

        assertNotNull(tree);
        assertTrue(tree.isEmpty());
    }

    @Test
    public void shouldCreateAnEmptyTreeFromAnEmptyQueue() {
        Tree tree = Tree.createTree(new LinkedList<Integer>());

        assertNotNull(tree);
        assertTrue(tree.isEmpty());
    }

    @Test
    public void shouldCreateATreeFromAListContainingOneElement() {
        final List<Integer> expected = Collections.unmodifiableList(
                Arrays.asList(1)
        );
        final Deque<Integer> deque = new LinkedList<>(expected);
        Tree tree = Tree.createTree(deque);

        assertNotNull(tree);
        assertFalse(tree.isEmpty());
        assertEquals(expected, tree.getTreeInLevelOrder());
    }

    @Test
    //@Ignore
    public void shouldCreateATreeFromAListContainingTwoElements() {
        final List<Integer> expected = Collections.unmodifiableList(
                Arrays.asList(1, 2)
        );
        final Deque<Integer> deque = new LinkedList<>(expected);
        Tree tree = Tree.createTree(deque);

        assertNotNull(tree);
        assertFalse(tree.isEmpty());
        assertEquals(expected, tree.getTreeInLevelOrder());
    }

    @Test
    //@Ignore
    public void shouldCreateATreeFromAListContainingThreeElements() {
        final List<Integer> expected = Collections.unmodifiableList(
                Arrays.asList(1, 2, 3)
        );
        final Deque<Integer> deque = new LinkedList<>(expected);
        Tree tree = Tree.createTree(deque);

        assertNotNull(tree);
        assertFalse(tree.isEmpty());
        assertEquals(expected, tree.getTreeInLevelOrder());
    }

    @Test
    //@Ignore
    public void shouldCreateATreeFromAListContainingFourElements() {
        final List<Integer> expected = Collections.unmodifiableList(
                Arrays.asList(1, 2, 3, 4)
        );
        final Deque<Integer> deque = new LinkedList<>(expected);
        Tree tree = Tree.createTree(deque);

        assertNotNull(tree);
        assertFalse(tree.isEmpty());
        assertEquals(expected, tree.getTreeInLevelOrder());
    }

    @Test
    //@Ignore
    public void shouldCreateATreeFromAListContainingFiveElements() {
        final List<Integer> expected = Collections.unmodifiableList(
                Arrays.asList(1, 2, 3, 4, 5)
        );
        final Deque<Integer> deque = new LinkedList<>(expected);
        Tree tree = Tree.createTree(deque);

        assertNotNull(tree);
        assertFalse(tree.isEmpty());
        assertEquals(expected, tree.getTreeInLevelOrder());
    }

    @Test
    //@Ignore
    public void shouldCreateATreeFromAListContainingSixElements() {
        final List<Integer> expected = Collections.unmodifiableList(
                Arrays.asList(1, 2, 3, 4, 5, 6)
        );
        final Deque<Integer> deque = new LinkedList<>(expected);
        Tree tree = Tree.createTree(deque);

        assertNotNull(tree);
        assertFalse(tree.isEmpty());
        assertEquals(expected, tree.getTreeInLevelOrder());
    }

    @Test
    //@Ignore
    public void shouldCreateATreeFromAListContainingSevenElements() {
        final List<Integer> expected = Collections.unmodifiableList(
                Arrays.asList(1, 2, 3, 4, 5, 6, 7)
        );
        final Deque<Integer> deque = new LinkedList<>(expected);
        Tree tree = Tree.createTree(deque);

        assertNotNull(tree);
        assertFalse(tree.isEmpty());
        assertEquals(expected, tree.getTreeInLevelOrder());
    }

    @Test
    //@Ignore
    public void shouldCreateATreeFromAListContainingEightElements() {
        final List<Integer> expected = Collections.unmodifiableList(
                Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8)
        );
        final Deque<Integer> deque = new LinkedList<>(expected);
        Tree tree = Tree.createTree(deque);

        assertNotNull(tree);
        assertFalse(tree.isEmpty());
        assertEquals(expected, tree.getTreeInLevelOrder());
    }

    @Test
    //@Ignore
    public void shouldCreateATreeFromAListContainingNineElements() {
        final List<Integer> expected = Collections.unmodifiableList(
                Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9)
        );
        final Deque<Integer> deque = new LinkedList<>(expected);
        Tree tree = Tree.createTree(deque);

        assertNotNull(tree);
        assertFalse(tree.isEmpty());
        assertEquals(expected, tree.getTreeInLevelOrder());
    }

    @Test
    //@Ignore
    public void shouldCreateATreeFromAListContainingTenElements() {
        final List<Integer> expected = Collections.unmodifiableList(
                Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        );
        final Deque<Integer> deque = new LinkedList<>(expected);
        Tree tree = Tree.createTree(deque);

        assertNotNull(tree);
        assertFalse(tree.isEmpty());
        assertEquals(expected, tree.getTreeInLevelOrder());
    }

    @Test
    //@Ignore
    public void shouldCreateATreeFromAListContainingProvidedElements() {
        final List<Integer> expected = Collections.unmodifiableList(
                Arrays.asList(4, 5, 9, 8, 6, 5, 2, 7, 8)
        );
        final Deque<Integer> deque = new LinkedList<>(expected);
        Tree tree = Tree.createTree(deque);

        assertNotNull(tree);
        assertFalse(tree.isEmpty());
        assertEquals(expected, tree.getTreeInLevelOrder());
    }

    @Test
    public void shouldReturnAnEmptyListFromAnEmptyTree() {
        Tree tree = Tree.createTree(new LinkedList<Integer>());

        List<Integer> actual = tree.findLevels(10);

        assertNotNull(actual);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void shouldReturnAnEmptyListIfThereIsNoSuchElementInTheTree() {
        final List<Integer> expected = Collections.unmodifiableList(
                Arrays.asList(1, 2, 3, 4, 5, 6, 7)
        );
        final Deque<Integer> deque = new LinkedList<>(expected);
        Tree tree = Tree.createTree(deque);

        List<Integer> actual = tree.findLevels(10);

        assertNotNull(actual);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void shouldReturnAListWithOneElementIfThereIsSuchAnElementInTheTree() {
        final List<Integer> expected = Collections.unmodifiableList(
                Arrays.asList(1, 2, 3, 4, 5, 6, 7)
        );
        final Deque<Integer> deque = new LinkedList<>(expected);
        Tree tree = Tree.createTree(deque);

        List<Integer> actual = tree.findLevels(7);

        assertNotNull(actual);
        assertFalse(actual.isEmpty());
        assertTrue(actual.size() == 1);
        assertEquals(3, actual.get(0).intValue());
    }

    @Test
    public void shouldPassTheProvidedTest() {
        final Deque<Integer> deque = new LinkedList<>(
                Arrays.asList(4, 5, 9, 8, 6, 5, 2, 7, 8)
        );
        Tree tree = Tree.createTree(deque);

        List<Integer> actual = tree.findLevels(5);

        assertNotNull(actual);
        assertFalse(actual.isEmpty());
        assertTrue(actual.size() == 2);

        final List<Integer> expected = Arrays.asList(2, 3);
        assertEquals(expected, actual);
    }
}
