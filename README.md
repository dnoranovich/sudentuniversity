# Students and University

A simple Java EE application to store students data, courses data and to 
enroll students for courses. The application uses MySql as a RDMS and JDBC
as a persistence layer. The use of EJBs relieves one from manually managing 
transactions and dealing with multi-threading issues. JSF/Primefaces is
used as a front-end technology.

The scripts to create and populate the database tables are in the *Scripts.sql* 
file in the project's folder.

A brief description of the application can be found [here.](https://www.youtube.com/watch?v=TGZ6fyuM-6o)


Deploying to Glassfish
---

The application in the *master* branch is intended to be deployed to
[Glassfish 4.x](https://glassfish.java.net/download.html) application server. So, it should be installed and the folder
*<Glassfish installation folder>/glassfish/bin* should be added to the path. 
To create a *datasource* one should execute the following from the main project's
folder.

~~~
asadmin add-resources src/main/webapp/WEB-INF/glassfish-resources.xml
~~~

All the database connection settings such as password are in the aforementioned 
*glassfish-resources.xml* file. The application can be deployed using Glassfish 
administrative interface accessible via URL [localhost:4848](localhost:4848).

The application can be accessed using 
[http://localhost:8080/QuickTapSurvey/faces/index.xhtml](http://localhost:8080/QuickTapSurvey/faces/index.xhtml) URL from the browser.


Deploying to Payara Micro
---

It takes time and effort to deploy application to Glassfish. So, it could be 
more convenient to launch as an executable fat jar file. To accomplish this task 
one can rely on [Payara Micro](http://www.payara.fish/payara_micro). The idea is 
that by adding a special Maven plugin Maven builds a war file based on which a 
jar file is created.

To try the executable jar file deployment, one should switch to *payara* branch 
of the repository.

~~~
git checkout payara
~~~

All the database connection settings in this branch are provided by the 
*@DataSourceDefinition* annotation on the UtilitySessionBean class in 
*(src/main/java/com/javaeeeee/quicktapsurvey/businesslogic)* folder.

~~~
@DataSourceDefinition(url = "jdbc:mysql://localhost:3306/quicktapsurvey?zeroDateTimeBehavior=convertToNull",
        name = "java:global/myDatasource",
        user = "your_login",
        password = "your_password",
        className = "com.mysql.jdbc.jdbc2.optional.MysqlDataSource")
~~~

To build the project one should execute the following

~~~
mvn clean install
~~~

and to launch the application execute the command below.

~~~
java -jar target/QuickTapSurvey-1.0-SNAPSHOT.jar
~~~

The application will be accessible in the browser by typing the 
[localhost:8080/QuickTapSurvey-1.0-SNAPSHOT/faces/index.xhtml](localhost:8080/QuickTapSurvey-1.0-SNAPSHOT/faces/index.xhtml) URL.

To stop the application one should press Ctrl+C.